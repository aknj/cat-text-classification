#!/usr/bin/env python
# -*- coding: utf-8 -*

# creates classifier
import os
import json
import random
import math
import numpy as np
import sys

def filter_page_object(json_object):
  # remove unwanted keys
  new_object = json_object
  new_object.pop('url')
  new_object.pop('meta')
  new_object.pop('comments_found')
  # new_object.pop('img')
  return new_object

def populate_dataset():
  path_to_files = './parser-output-all'
  files = list(map(lambda filename: open(path_to_files+'/'+filename).read(),
                                    os.listdir(path_to_files)))

  # first we shuffle the pages, then we extract class to target and ev else to data

  random.shuffle(files)

  data = []
  target = []

  for f in files:
    object = json.loads(f)
    target.append(object.pop('class'))

    # optionally remove unwanted keys
    object = filter_page_object(object)

    data_string = ' '.join(filter(None, map(lambda key: object.get(key), object.keys())))
    data.append(data_string)

  return data, target


def get_train_and_test():
  data, target = populate_dataset()

  train = {
    'data': [],
    'target': []
  }

  train_len = math.floor(0.8 * len(data))
  train['data'] = data[:train_len]
  train['target'] = target[:train_len]

  test = {
    'data': [],
    'target': []
  }

  train_len = math.floor(0.8 * len(data))
  test['data'] = data[train_len:]
  test['target'] = target[train_len:]

  return train, test


def get_classifier(train):
  from sklearn.feature_extraction.text import CountVectorizer
  import nltk

  # pattern = r'(?u)\b\w\w+\b'
  pattern = r'(?u)\b[a-zA-Z][a-zA-Z]+\b'

  count_vect = CountVectorizer(
    ngram_range=(1, 2),
    # strip_accents='unicode',
    stop_words='english',
    min_df=0.1,
    max_df=0.9,
    # max_features=100,
    token_pattern=pattern
  )


  from sklearn.feature_extraction.text import TfidfTransformer

  from sklearn.naive_bayes import MultinomialNB

  from sklearn.pipeline import Pipeline
  text_clf = Pipeline([('vect', count_vect),
                      ('tfidf', TfidfTransformer()),
                      ('clf', MultinomialNB(alpha=.1))
  ])

  text_clf = text_clf.fit(train['data'], train['target'])
  # print('feature_names:', text_clf.steps[0][1].get_feature_names())
  # print(text_clf.steps[0][1].vocabulary_)


  return text_clf


usage = """ usage:
        classifiactor.py test
        classifiactor.py final [<output_filename>]
        classifiactor.py url <url_string>
        """

if len(sys.argv) > 1:
  train, test = get_train_and_test()
  text_clf = get_classifier(train)
  print('pipeline:', text_clf)
  print('')

  if sys.argv[1] == 'test':
    predicted = text_clf.predict(test['data'])

    score = np.mean(predicted == test['target'])
    print(score)
    scores = [score]

    # print('Score:', text_clf.score(test['data'], test['target']))

    for i in range(9):
      train, test = get_train_and_test()
      text_clf = get_classifier(train)
      predicted = text_clf.predict(test['data'])
      score = text_clf.score(test['data'], test['target'])
      scores.append(score)
      print(score)

    print('')
    print(np.mean(scores))

    from sklearn import metrics
    print('')
    print(metrics.classification_report(test['target'], predicted,
        target_names=['cats', 'funny_cats', 'random']))

  elif sys.argv[1] == 'final':
    if len(sys.argv) == 2:
      filename = 'finalized_model_default.sav'
    else:
      filename = sys.argv[2]

    print('saving model to file', filename)
    import pickle
    pickle.dump(text_clf, open(filename, 'wb'))

  elif sys.argv[1] == 'url':
    if len(sys.argv) > 2:
      url = sys.argv[2]
      from page_get import get_parsed_page

      page_data = get_parsed_page(url)
      page_object = json.loads(page_data)

      # optionally remove unwanted keys
      page_object = filter_page_object(page_object)

      data_string = ' '.join(filter(None, map(lambda key: page_object.get(key), page_object.keys())))

      guess = text_clf.predict([data_string])
      # guess_proba = text_clf.predict_proba([data_string])
      # guess_log_proba = text_clf.predict_log_proba([data_string])

      print('My guess is:', guess[0])
      # print('Probabilities:', guess_proba)
      # print('Log probabilities:', guess_proba)

    else:
      print('provide url')

  else:
    print(usage)

else:
  print(usage)