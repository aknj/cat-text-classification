import urllib.request
import urllib.error
import json
from html_parser import Parsed_HTML

user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers = {'User-Agent': user_agent, }

def get_parsed_page(url):
  request = urllib.request.Request(url, None, headers)
  try:
      page = urllib.request.urlopen(request)
  except urllib.error.__all__:
      pass
  html = page.read().decode('utf-8')
  parsed_html = Parsed_HTML(html, url)


  data = parsed_html.pack_json()
  serialized = json.dumps(data)
  return serialized
