# from text import html_doc, simple,joe, bbc
# from text2 import brit
import re
import json
from bs4 import BeautifulSoup
import sys
from pymongo import MongoClient

com = ["Comments", "comments", "comment", "Comment"]
img_attr = ["title", "src", "alt", "width", "height"]
meta_wanted = ['keywords', 'description', "og:type", "og:title", "og:site_name"]

class Parsed_HTML():

    def __init__(self, html_string, url):
        #self.title
        self.url = url
        self.html_file = html_string
        self.meta = {}
        self.images = []
        self.soup = BeautifulSoup(html_string, 'html.parser')
        self.article = False
        self.article_content =""
        self.comments = None
        self.comments_found = False
        self.title = ""
        self.title_in_txt = ""
        self.find_content()

    def find_content(self):
        self.find_title()
        self.find_meta()
        self.find_comments()
        self.find_article()
        self.filtr_article_content()
        '''print("TITLE")
        print(self.title)
        print("Meta")
        print(self.meta)
        print("Comments")
        print(self.comments)
        print("Img")
        print(self.images)
        print("Comments")
        print(self.comments)
        print("Articel content")'''
        # print('Article content:', self.article_content)
        # self.analyze()
    def find_title(self):
        self.title = str(self.soup.title.string)

    def find_meta(self):
        for m in meta_wanted:
            if self.soup.find("meta", {"name":m}) != None:
                self.meta[m] = str(self.soup.find("meta", {"name":m})['content'])
            elif self.soup.find("meta", {"property":m}) != None:
                self.meta[m] = str(self.soup.find("meta", {"property":m})['content'])
        if  "og:type" in self.meta:
            self.article = (self.meta["og:type"] == 'article')


    #TO DO FIND COMMENTS
    def find_article(self):
        if (self.soup.find('article')):
            self.article = True
            for script in self.soup.article(["script", "style"]):
                script.extract()
            #FIND IMAGES
            for i in self.soup.article.find_all("img"):
                attr = []
                for a in img_attr:
                    attr.append(i.get(a))
                self.images.append(attr)
            self.article_content = self.convert_to_plain_text(self.soup.article.get_text())

        else:
            for script in self.soup(["script", "style"]):
                script.extract()
            self.title_in_txt = self.find_real_title(self.title, self.meta, self.url)
            if self.title_in_txt != -1:
                #print("PRAWDZIWY TYTUŁ")
                #print(self.title_in_txt)
                #finding specific string in text: title
                #body = soup.html
                #self.title_in_txt = "gagag]"
                for i in self.soup.find_all(text=self.title_in_txt):
                    if i.parent != None and i.parent.name == 'title':
                        pass
                    else:
                        if i.parent.parent!=None:
                            self.article_content = self.convert_to_plain_text(i.parent.parent.get_text())
                            for i in i.parent.parent.find_all("img"):
                                self.images.append([i.get("title"), i.get("src"), i.get("alt")])
                        #article to plain text

            #cannot find article content'''
        if len(self.article_content) < len(self.title)+50:
                for i in self.soup.find_all("img"):
                    self.images.append([i.get("title"), i.get("src"), i.get("alt")])
                for script in self.soup(["script", "style"]):
                    script.extract()
                self.article_content = self.convert_to_plain_text(self.soup.get_text())
                self.try_to_cut()

    def try_to_cut(self):
        if len(self.title) > 0:
            if len(self.title_in_txt)==0:
                self.title_in_txt = self.title
            index = self.article_content.find(self.title)
            if index !=1:
                self.article_content=self.article_content[index:]

    def convert_to_plain_text(self, text):
        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line eachs
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)
        return text


    def find_common_part(self, string1, string2):
        answer = ""
        len1, len2 = len(string1), len(string2)
        for i in range(len1):
            match = ""
            for j in range(len2):
                if (i + j < len1 and string1[i + j] == string2[j]):
                    match += string2[j]
                else:
                    if (len(match) > len(answer)): answer = match
                    match = ""
        return answer

    def find_real_title(self, title, meta, url):
        if 'og:site_name' in meta:
            site_name = meta['og:site_name']
        else:
            site_name = -1
        if site_name != -1:
            if len(title) > len(site_name) - 5 and site_name in title:
                max_len = len(title) - len(site_name) - 5
                return title[:max_len]
        title_with_ = url.replace(" ", "_")
        title_with_minus = url.replace(" ", "-")
        e1 = self.find_common_part(title, title_with_)
        if len(e1) > 3:
            return e1.replace("_", " ")
        else:
            e1 = self.find_common_part(title, title_with_minus)
            if len(e1) > 3:
                return e1.replace("-", " ")
            else:
                return ""

#TO DO - DECOMPOSE COMMENTS SEPARATE THEM - LOOK FOR THE STRUCTURE OR SEPARATE BY DATE WITH REGEX
    def find_comments(self):
        for i in com:
            if self.soup.find(text=i):
                if self.soup.find(text=i).parent.parent != None:
                    self.comments = self.convert_to_plain_text(self.soup.find(text=i).parent.parent.get_text())
                    #print(self.comments)
                    self.soup.find(text=i).parent.parent.decompose()
                    self.contain_comments = True
                    break

    def filtr_article_content(self):
        self.article_content = re.sub("\n\w+\s*\n", "\n", str(self.article_content))
        #self.article_content = re.sub("\n\S\n", "\n", str(self.article_content))
        '''not_approved = ['Share this with*', 'WhatsApp', 'Pinterest', 'Messenger', 'LinkedIn', 'Copy this link', 'Facebook', 'Email']
        string = self.article_content.splitlines()
        for i in string:
            for no in not_approved:
                i = re.sub(no,"",i)

        self.article_content = "\n".join(string)'''
        # self.article_len = print(len(self.article_content))

    def analyze(self):
        words = ["Cat", "cat", "kitt", "Kitt", "pet", "animal", "fun", "laugh", "crazy", "image"]
        self.article_words = len(self.article_content.split())
        self.number_of_words = [] # tu są zliczone słowa z tej tablicy words ike razy występują w tekście o takich samych indeksach
        self.img_alt = [0]* len(words) # tu to samo dla każdego obrazka i w każdym elemencie jest suma zliczonych z wszystkich obrazków tych słów
        self.nr_of_img = len(self.images)
        for i in words:
            #reg = "\s*\w"+i+"*\w\s"
            self.number_of_words.append(len(re.findall(i, self.article_content)))
        for img in self.images:
            for i in range(len(words)):
                if img[2] != None:
                    self.img_alt[i] += (len(re.findall(words[i], img[2])))
        print('img_alt', self.img_alt)
        print('nr_of_img', self.nr_of_img)
    def pack_json(self):
        data = {}
        data['url'] = self.url
        data['title'] = self.title
        data['meta'] = str(self.meta)
        data['article_content'] = self.article_content
        data['comments_found'] = str(self.comments_found)
        data['comments'] = self.comments
        data['img'] = str(self.images)

        # print('url: ', data['url'])
        # print('title: ', data['title'])
        # print('meta: ', data['meta'])
        # print('article_content: ', data['article_content'])
        # print('comments_found: ', data['comments_found'])
        # print('comments: ', data['comments'])
        # print('img: ', data['img'])

        #print(data)
        return data
#print("AFAF")
#Parsed_HTML(joe, 'https://learnenglish.britishcouncil.org/')
'''

def parse_html(html, url):
    #html = bbc
    x = Parsed_HTML(html, url)
    print(put_into_db(x.pack_json()))

def put_into_db(json):
    db_name = "IDK"
    col_name = "parsed_html"
    MONGO_URL = "mongodb://localhost:27017/"

    client = MongoClient(MONGO_URL)
    db = client.admin
    db = client[db_name]
    db_col = db[col_name]
    print(json)
    return db_col.insert_one(json)

#html = bbc
#parse_html(html_doc, 'https://learnenglish.britishcouncil.org/')

def get_data_from_mongodb():
    db_name = "IDK"
    col_name = "parsed_html"
    MONGO_URL = "mongodb://localhost:27017/"

    client = MongoClient(MONGO_URL)
    db = client.admin
    db = client[db_name]
    db_col = db[col_name]

    for x in db_col.find():
        print(x)
#parse_data_from_mongodb()'''
