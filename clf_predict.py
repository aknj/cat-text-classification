import os
import pickle
import json
from page_get import get_parsed_page

model_filename = 'finalized_model.sav'
text_clf = pickle.load(open(model_filename, 'rb'))


def filter_page_object(json_object):
  # remove unwanted keys
  new_object = json_object
  new_object.pop('url')
  new_object.pop('meta')
  new_object.pop('comments_found')
  # new_object.pop('img')
  return new_object

def guess_category(json_object):
  page_object = filter_page_object(json_object)
  print(text_clf)

  data_string = ' '.join(filter(None, map(lambda key: page_object.get(key), page_object.keys())))

  # print(text_clf.predict([data_string])[0])
  prediction = text_clf.predict([data_string])[0]
  if prediction == 'cats':
    return 1
  elif prediction == 'funny_cats':
    return 2
  else:
    return 3
