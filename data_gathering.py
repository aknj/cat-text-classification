import os
import redis
import urllib.request
import urllib.error
from hdfs import InsecureClient, util

from html_parser import ParsedHTML, store_site_data, get_site_data
from clf_predict import guess_category

url_path = './urls'

r = redis.Redis(
    host='localhost',
    port=6379,
    decode_responses=True)

client = InsecureClient('http://localhost:9870')

user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers = {'User-Agent': user_agent, }


def gather_urls():
    urls = []

    for fn in os.listdir(url_path):
        print(fn)
        fn = os.path.join(url_path, fn)
        print(fn)
        with open(fn) as f:
            f.readline()
            for line in f:
                url = line.rstrip('\n').split(';')[1].strip('"')
                # print(url)
                if url not in urls:
                    urls.append(url)
    print(len(urls))

    fn = os.path.join(url_path, 'all_urls.csv')
    with open(fn, 'w') as f:
        f.write(str(len(urls)) + '\n')
        for elem, i in zip(urls, range(len(urls))):
            f.write(str(i) + ':' + elem + '\n')


def integrate_training_urls():
    failed_urls, broken_urls = [], []
    f1_name = os.path.join(url_path, 'all_urls.csv')
    with open(f1_name, 'r+') as f1:
        for line in f1:
            pass
        i = int(line.split(':')[0]) + 1
        last_id = i
        for f2_namepart in ['cats.txt', 'funny_cats.txt', 'random.txt']:
            f2_name = os.path.join(url_path, 'training_data/' + f2_namepart)
            with open(f2_name) as f2:
                try:
                    for line in f2:
                        url = line.strip()
                        if r.get(url) is None:
                            failed, broken = write_to_hdfs(i, url)
                            if failed:
                                failed_urls += failed
                            if broken:
                                broken_urls += broken
                            f1.write(str(i) + ':' + url + '\n')
                            i += 1
                            if i - last_id % 5 == 0:
                                print('file: ' + f2_namepart)
                                print('last url: ' + url)
                                print('at: ' + str(i - last_id) + '/150')

                except KeyboardInterrupt:
                    with open('failed_broken.txt', 'a+') as f:
                        f.write('failed urls:')
                        for url in failed_urls:
                            f.write(url)
                        f.write('\nbroken urls:')
                        for url in broken_urls:
                            f.write(url)

    with open('failed_broken.txt', 'a+') as f:
        f.write('failed urls:')
        for url in failed_urls:
            f.write(url)
        f.write('\nbroken urls:')
        for url in broken_urls:
            f.write(url)


def clear_missing_records_from_redis():
    cleared = []
    i = 0
    keys = r.keys()
    url_count = len(keys)
    for url in r.keys():
        try:
            read_from_hdfs(url)
        except (util.HdfsError, UnicodeDecodeError):
            try:
                delete_from_hdfs(url)
            except util.HdfsError:
                pass
            val = r.hget(url, 'filename')
            r.delete(url)
            print('CLEARED: ' + url)
            cleared.append([url, val])

        i += 1
        if i % 5 == 0:
            print('last url: ' + url)
            print('at: ' + str(i) + '/' + str(url_count))

    for elem in cleared:
        print(str(elem[1]) + ': ' + str(elem[0]))


def clear_wrong_keytype_records():
    cleared = []
    i = 0
    keys = r.keys()
    url_count = len(keys)
    for url in r.keys():
        try:
            r.hget(url, 'filename')
        except redis.exceptions.ResponseError:
            filename = r.get(url)
            client.delete('/data/' + filename)
            r.delete(url)
            print('CLEARED: ' + str(filename) + ', ' + url)
            cleared.append([url, filename])

        i += 1
        if i % 5 == 0:
            print('last url: ' + url)
            print('at: ' + str(i) + '/' + str(url_count))

    for elem in cleared:
        print(str(elem[1]) + ': ' + str(elem[0]))


def download_sites_to_hdfs():
    failed_urls, broken_urls = [], []
    fn = os.path.join(url_path, 'all_urls.csv')
    urls = r.keys()
    ids = []
    for url in urls:
        print(url)
        ids.append(r.hget(url, 'filename'))
    with open(fn) as f:
        url_count = int(f.readline())
        for line in f:
            line.strip().split(':', 1)
            i, url = line.strip().split(':', 1)
            i = int(i)
            if url not in urls:
                site_id = get_id(ids)
                failed, broken = write_to_hdfs(site_id, url)
                if failed:
                    failed_urls += failed
                elif broken:
                    broken_urls += broken
                else:
                    print(url, type(url))
                    print(site_id, type(site_id))
                    r.hset(url, 'filename', str(site_id))
                    ids.append(str(site_id))
                    print('ADDED RECORD: ' + str(site_id) + ', ' + url)

            if i > 0 and i % 5 == 0:
                print('last url: ' + url)
                print('at: ' + str(i) + '/' + str(url_count))

    with open('failed_broken.txt', 'a+') as f:
        f.write('failed urls:')
        for i, url in failed_urls:
            f.write(str(i) + ', ' + url)
        f.write('\nbroken urls:')
        for i, url in broken_urls:
            f.write(str(i) + ', ' + url)
        print('failed:')
        print(failed_urls)
        print('broken:')
        print(broken_urls)


def extract_data_from_hdfs_dataset():
    urls = r.keys()
    i = 0
    url_count = len(urls)
    for url in urls:
        html = None
        try:
            html = read_from_hdfs(url)
        except UnicodeDecodeError:
            delete_from_hdfs(url)
            r.delete(url)
        if html is None:
            print('NONE: ' + url)
            continue
        try:
            parsed_html = ParsedHTML(html, url)
        except ValueError:
            print('xxx')
            i += 1
            if i % 5 == 0:
                print('last url: ' + url)
                print('at: ' + str(i) + '/' + str(url_count))
            continue

        site_data_id = store_site_data(parsed_html.pack_json())
        print(site_data_id)
        r.hset(url, 'site_data_id', str(site_data_id))
        i += 1
        if i % 5 == 0:
            print('last url: ' + url)
            print('at: ' + str(i) + '/' + str(url_count))


def clear_data_ids_from_redis():
    keys = r.keys()
    for key in keys:
        r.hdel(key, 'data_id')


def delete_from_hdfs(url):
    filename = r.hget(url, 'filename')
    client.delete('/data/' + filename)


def write_to_hdfs(i, url):
    try:
        request = urllib.request.Request(url, None, headers)
        try:
            page = urllib.request.urlopen(request)
        except urllib.error.__all__:
            return [i, url], None
        try:
            html = page.read()
        except UnicodeDecodeError:
            return None, [i, url]
        with client.write('/data/' + str(i)) as writer:
            writer.write(html)
        r.set(url, i)
    except Exception:
        return [i, url], None
    return None, None


def read_from_hdfs(url):
    filename = r.hget(url, 'filename')
    if filename is None:
        return None
    with client.read('/data/' + filename) as reader:
        html = reader.read().decode('utf-8')
        return html


def get_id(keys):
    i = 0
    while str(i) in keys:
        i += 1
    return i


def classify_site(url):
    data_id = r.hget(url, 'data_id')
    if data_id is None:
        request = urllib.request.Request(url, None, headers)
        try:
            page = urllib.request.urlopen(request)
        except urllib.error.__all__:
            raise ValueError
        try:
            html = page.read().decode('utf-8')
        except UnicodeDecodeError:
            raise ValueError
    else:
        html = get_site_data(data_id)
    parsed_html = ParsedHTML(html, url)
    return guess_category(parsed_html.pack_json())


def main():
    # clear_data_ids_from_redis()
    extract_data_from_hdfs_dataset()


    # request = urllib.request.Request(url, None, headers)
    # page = urllib.request.urlopen(request)
    # html = page.read().decode('utf-8')
    # parsed_html = ParsedHTML(html, url)

    # fname = os.path.join(url_path, 'training_data/cats.txt')
    # with open(fname) as f:
    #     line = f.readline()
    # url = line.strip()
    # print(url)
    # html = read_from_hdfs(url)
    # parsed_html = ParsedHTML(html, url)
    # jsonified = parsed_html.pack_json()
    # store_site_data(jsonified)


    # with open(fn) as f:
    #     for line in f:
    #         i, url = line.strip().split(':', 1)
    #         i = int(i)
    #         if i < 5:
    #             html = read_from_hdfs(url)
    #             print(html)


    # for url, i in zip(urls, range(len(urls))):
    #     r.set(url, i)
    #     if i < 5:
    #         request = urllib.request.Request(url, None, headers)
    #         page = urllib.request.urlopen(request)
    #         try:
    #             html = page.read()
    #         except UnicodeDecodeError:
    #             continue
    #         with client.write('/data/' + str(i)) as writer:
    #             writer.write(html)
    #
    # for url, i in zip(urls, range(len(urls))):
    #     if i < 5:
    #         html = read_from_hdfs(url)
    #         print(html)


if __name__ == '__main__':
    main()
