import os
import redis
import urllib.request
import urllib.error
import json
from html_parser import Parsed_HTML
import sys

url_path = './urls'

user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
headers = {'User-Agent': user_agent, }


def extract_data_from_training_urls():
    i = 0   # page counter

    # directory suffix can be provided as an argument
    if len(sys.argv) > 1:
        output_path = 'parser-output-'+sys.argv[1]
    else:
        output_path = 'parser-output-all'
    if not os.path.exists(output_path):
        os.makedirs(output_path)


    for f2_namepart in ['cats.txt', 'funny_cats.txt', 'random.txt']:
        f2_name = os.path.join(url_path, 'training_data/' + f2_namepart)
        with open(f2_name) as f2:
            for line in f2:
                i += 1
                print(i)
                url = line.strip()
                request = urllib.request.Request(url, None, headers)
                try:
                    page = urllib.request.urlopen(request)
                except urllib.error.URLError as e:
                    print(url)
                    print(e)
                    continue
                html = page.read().decode('utf-8')
                parsed_html = Parsed_HTML(html, url)

                data = parsed_html.pack_json()
                # add category (class) column for classification
                data['class'] = f2_namepart[:-4]

                # site_data_id = put_into_db(data)

                # serialize pack_json() output and write each page object
                # to a separate file in <output_path> directory

                output_filename = 'page-' + format(i, '05') + '.json'
                output_file_path = os.path.join(output_path, output_filename)
                output_file = open(output_file_path, "w+")

                json.dump(data, output_file, indent=0)


extract_data_from_training_urls()
